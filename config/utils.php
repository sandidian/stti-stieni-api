<?php
return [
    'type_category' => [
        'article' => 1,
        'event' => 2,
        'gallery' => 3,
    ],
    'type_tag' => [
        'article' => 1,
    ],
    'status_post' => [
        'Publish' => 1,
        'Draft' => 2,
        'Archive' => 3,
    ],
]

?>
