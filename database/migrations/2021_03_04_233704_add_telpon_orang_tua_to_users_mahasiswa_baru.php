<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTelponOrangTuaToUsersMahasiswaBaru extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_mahasiswa_baru', function (Blueprint $table) {
            $table->string('phone_orang_tua', 30)->after('kode_pos_orangtua')->nullable();
            $table->string('no_telp_orang_tua', 30)->after('phone_orang_tua')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_mahasiswa_baru', function (Blueprint $table) {
            //
        });
    }
}
