<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeBiayaToUsersMahasiswaPrimagamas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_mahasiswa_primagamas', function (Blueprint $table) {
            $table->integer('biaya_rencdari')->change()->nullable();
            $table->integer('biaya_spb')->change()->nullable();
            $table->integer('biaya_spp')->change()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_mahasiswa_primagamas', function (Blueprint $table) {
            //
        });
    }
}
