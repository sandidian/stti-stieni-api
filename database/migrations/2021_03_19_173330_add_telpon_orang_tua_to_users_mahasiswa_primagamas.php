<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTelponOrangTuaToUsersMahasiswaPrimagamas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_mahasiswa_primagamas', function (Blueprint $table) {
            $table->string('phone_orang_tua', 30)->after('alamat_pekerjaan_orangtua')->nullable();
            $table->string('no_telp_orang_tua', 30)->after('phone_orang_tua')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_mahasiswa_primagamas', function (Blueprint $table) {
            //
        });
    }
}
