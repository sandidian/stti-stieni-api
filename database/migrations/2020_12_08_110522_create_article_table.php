<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug')->index();
            $table->integer('id_category')->index();
            $table->json('tag')->nullable()->default(null);
            $table->text('content')->nullable()->default(null);
            $table->integer('author');
            $table->text('image');
            $table->integer('viewer')->default(0);;
            $table->timestamp('published_at')->nullable()->default(null);
            $table->integer('status')->comment("1: publish, 2: draft, 3: archive, 4: deleted")->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article');
    }
}
