<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug')->index();
            $table->integer('id_category')->index();
            $table->text('content')->nullable()->default(null);
            $table->string('author', 100);
            $table->string('location');
            $table->dateTime('date_start');
            $table->dateTime('date_end');
            $table->text('image');
            $table->timestamp('published_at')->nullable()->default(null);
            $table->integer('status')->comment("1: publish, 2: draft, 3: archive, 4: deleted")->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event');
    }
}
