<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersMahasiswaBaru extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_mahasiswa_baru', function (Blueprint $table) {
            $table->id();
            $table->string('nama_lengkap',50);
            $table->string('username',50)->unique();
            $table->string('email')->unique()->index();
            $table->string('phone',30);
            $table->string('no_telp',30)->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('tempat_lahir',50);
            $table->date('tanggal_lahir');
            $table->smallInteger('gender')->comment("1: Laki; 2: Perempuan");
            $table->smallInteger('agama')->nullable();
            $table->string('nik',20)->nullable();
            $table->text('alamat');
            $table->integer('provinsi_id');
            $table->integer('kota_id');
            $table->string('kode_pos',10);
            $table->smallInteger('pendidikan_terakhir');
            $table->string('nama_sekolah',50);
            $table->text('alamat_sekolah');
            $table->integer('provinsi_id_sekolah');
            $table->integer('kota_id_sekolah');
            $table->string('tahun_lulus',10)->nullable();
            $table->string('no_ijazah',25)->nullable();
            $table->string('pekerjaan',50)->nullable();
            $table->text('alamat_kantor')->nullable();
            $table->string('no_telp_kantor',30)->nullable();
            $table->string('ukuran_jaket',50)->nullable();
            $table->string('nama_ayah',50)->nullable();
            $table->string('nik_ayah',20)->nullable();
            $table->string('nama_ibu',50)->nullable();
            $table->string('nik_ibu',20)->nullable();
            $table->text('alamat_orangtua')->nullable();
            $table->integer('provinsi_id_orangtua')->nullable();
            $table->integer('kota_id_orangtua')->nullable();
            $table->string('kode_pos_orangtua',10)->nullable();
            $table->string('pekerjaan_ibu',25)->nullable();
            $table->string('pekerjaan_ayah',25)->nullable();
            $table->integer('penanggung_biaya')->nullable();
            $table->smallInteger('spp')->nullable();
            $table->smallInteger('spb')->nullable();
            // $table->string('jenjang_pendidikan',40)->nullable();
            $table->integer('kampus_studi')->nullable();
            $table->integer('program_studi')->nullable();
            $table->string('tipe_waktu_kuliah',40)->nullable();
            $table->string('hari_waktu_kuliah',40)->nullable();
            $table->string('jam_waktu_kuliah',40)->nullable();
            $table->smallInteger('sumber_info')->nullable();
            $table->text('foto')->nullable();
            $table->smallInteger('status')->comment("1: active, 2: not active")->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
