<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDataOrangtuaToUsersMahasiswaPrimagamas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_mahasiswa_primagamas', function (Blueprint $table) {
            $table->integer('status_pekerjaan')->after('asal_alamat_sekolah')->nullable();
            $table->string('nama_orang_tua', 50)->after('kode_pos')->nullable();
            $table->text('alamat_orangtua')->after('nama_orang_tua')->nullable();
            $table->integer('pekerjaan_orangtua')->after('alamat_orangtua')->nullable();
            $table->string('pekerjaan_orangtua_other', 25)->after('pekerjaan_orangtua')->nullable();
            $table->text('alamat_pekerjaan_orangtua')->after('pekerjaan_orangtua')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_mahasiswa_primagamas', function (Blueprint $table) {
            //
        });
    }
}
