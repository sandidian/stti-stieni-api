<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCompleteToUsersMahasiswaBaru extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_mahasiswa_baru', function (Blueprint $table) {
            $table->integer('complete')->after('status')->comment("1: complete, 2: not complete")->default(2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_mahasiswa_baru', function (Blueprint $table) {
            //
        });
    }
}
