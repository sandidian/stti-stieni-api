<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersMahasiswaPrimagamas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_mahasiswa_primagamas', function (Blueprint $table) {
            $table->id();
            $table->string('username',50)->unique();
            $table->string('nama_lengkap',50);
            $table->string('email')->unique()->index();
            $table->string('phone',30);
            $table->string('no_telp',30)->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('tempat_lahir',50);
            $table->date('tanggal_lahir');
            $table->integer('gender')->comment("1: Laki; 2: Perempuan");
            $table->text('alamat');
            $table->integer('provinsi_id');
            $table->integer('kota_id');
            $table->string('kode_pos',10);
            $table->string('nama_ibu',50)->nullable();
            $table->string('asal_lulusan',50)->nullable();
            $table->string('asal_jurusan',50)->nullable();
            $table->string('asal_alamat_sekolah',50)->nullable();
            // $table->integer('iskerja')->comment("1: Yes; 2: No");
            $table->string('kantor_instansi',50)->nullable();
            $table->string('kantor_jabatan',50)->nullable();
            $table->text('kantor_alamat')->nullable();
            $table->string('kantor_no_telp',30)->nullable();
            $table->string('biaya_rencdari',50)->nullable();
            $table->string('biaya_spb',50)->nullable();
            $table->string('biaya_spp',50)->nullable();
            $table->integer('sumber_info')->nullable();
            $table->string('wawancara',50)->nullable();
            $table->string('perguruan_tinggi',50)->nullable();
            $table->json('jurusan');
            // $table->integer('islunas')->comment("1: Belum; 2: Sudah");
            $table->integer('status')->comment("1: active, 2: not active")->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
