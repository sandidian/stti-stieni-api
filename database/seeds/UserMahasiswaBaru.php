<?php

use Illuminate\Database\Seeder;

class UserMahasiswaBaru extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_mahasiswa_baru')->insert([
            'username' => 'mahasiswa-baru',
            'nama_lengkap' => 'Mahasiswa Baru',
            'email' => 'mahasiswa-baru@gmail.com',
            'phone' => '0812312313223',
            'password' => Hash::make('Password123'),
            'tempat_lahir' => 'Jakarta',
            'tanggal_lahir' => '1999-06-11',
            'gender' => 1,
            'alamat' => 'Cisarua Bogor',
            'provinsi_id' => 2,
            'kota_id' => 4,
            'kode_pos' => 10,
            'pendidikan_terakhir'=> 1,
            'nama_sekolah' => 'SMK Wikrama Bogor',
            'alamat_sekolah' => 'Tajur Bogor',
            'provinsi_id_sekolah' => 2,
            'kota_id_sekolah' =>2,
            'status' => 1
        ]);
    }
}
