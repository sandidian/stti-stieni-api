<?php

use Illuminate\Database\Seeder;

class StudySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('study')->insert([
            [
                'name' => 'Management (S-1)',
                'image' => 'images/pages/content-img-2.jpg',
                'content' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur, temporibus! Architecto corrupti sint iusto quos rem reprehenderit, veniam doloribus, nesciunt, soluta nostrum voluptatibus doloremque ipsa hic reiciendis beatae. Iste, est.'
            ],
            [
                'name' => 'Akuntansi (S-1)',
                'image' => 'images/pages/content-img-2.jpg',
                'content' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur, temporibus! Architecto corrupti sint iusto quos rem reprehenderit, veniam doloribus, nesciunt, soluta nostrum voluptatibus doloremque ipsa hic reiciendis beatae. Iste, est.'
            ],
            [
                'name' => 'Teknik Mesin (S-1)',
                'image' => 'images/pages/content-img-2.jpg',
                'content' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur, temporibus! Architecto corrupti sint iusto quos rem reprehenderit, veniam doloribus, nesciunt, soluta nostrum voluptatibus doloremque ipsa hic reiciendis beatae. Iste, est.'
            ],
            [
                'name' => 'Teknik Elektro (S-1)',
                'image' => 'images/pages/content-img-2.jpg',
                'content' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur, temporibus! Architecto corrupti sint iusto quos rem reprehenderit, veniam doloribus, nesciunt, soluta nostrum voluptatibus doloremque ipsa hic reiciendis beatae. Iste, est.'
            ],
            [
                'name' => 'Teknik Informatika (S-1)',
                'image' => 'images/pages/content-img-2.jpg',
                'content' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur, temporibus! Architecto corrupti sint iusto quos rem reprehenderit, veniam doloribus, nesciunt, soluta nostrum voluptatibus doloremque ipsa hic reiciendis beatae. Iste, est.'
            ]
        
        ]);
    }
}
