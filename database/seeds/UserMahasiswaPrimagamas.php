<?php

use Illuminate\Database\Seeder;

class UserMahasiswaPrimagamas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_mahasiswa_primagamas')->insert([
            'username' => 'mahasiswa-primagamas',
            'nama_lengkap' => 'Mahasiswa Primagamas',
            'email' => 'mahasiswa-primagamas@gmail.com',
            'phone' => '0812312313223',
            'password' => Hash::make('Password123'),
            'tempat_lahir' => 'Jakarta',
            'tanggal_lahir' => '1999-06-11',
            'gender' => 1,
            'alamat' => 'Cisarua Bogor',
            'provinsi_id' => 2,
            'kota_id' => 4,
            'kode_pos' => 10,
            'jurusan' => json_encode([1, 2]),
            'status' => 1,
        ]);
    }
}
