@component('mail::layout')
{{-- Header --}}
@slot('header')
    @component('mail::header', ['url' => config('app.url')])
    	STTI-STIENI
    @endcomponent
@endslot
{{-- Body --}}
Hallo **{{$nama_lengkap}}**,  {{-- use double space for line break --}}
Selamat Anda sudah terdaftar di website kami sebagai Mahasiswa Baru.  
Gunakan email & password dibawah ini untuk melakukan login kehalaman dashboard.  
  
Email : **{{$email}}**  
Password : **{{$password}}**  
  
Setelah login Anda bisa mengganti password di halaman reset password.  
@component('mail::button', ['url' => $link])
Login
@endcomponent
{{-- Subcopy --}}
@isset($subcopy)
    @slot('subcopy')
        @component('mail::subcopy')
            {{ $subcopy }}
        @endcomponent
    @endslot
@endisset
{{-- Footer --}}
@slot('footer')
    @component('mail::footer')
        © {{ date('Y') }} STTI-STIENI
    @endcomponent
@endslot
@endcomponent