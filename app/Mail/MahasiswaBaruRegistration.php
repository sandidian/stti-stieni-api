<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MahasiswaBaruRegistration extends Mailable
{
    use Queueable, SerializesModels;

    private $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $link = env('URL_FRONTEND_APP_MAIN') . "/smb/login";

        return $this->subject('Daftar Mahasiswa Baru Berhasil')
            ->from('stti-stieni@email.com', 'STTI-STIENI')
            ->markdown('emails.mahasiswa-baru.register')
            ->with([
                'password' => $this->data['password'],
                'nama_lengkap' => $this->data['nama_lengkap'],
                'email' => $this->data['email'],
                'link' => $link,
            ]);
    }
}
