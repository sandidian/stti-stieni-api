<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MahasiswaPrimagamasRegistration extends Mailable
{
    use Queueable, SerializesModels;

    private $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $link = env('URL_FRONTEND_APP_PRIMAGAMAS') . "/login";

        return $this->subject('Registrasi Berhasil')
            ->from('vokasi@sttstieni.ac.id', 'STTI-STIENI-PRIMAGAMAS')
            ->markdown('emails.mahasiswa-primagamas.register')
            ->with([
                'password' => $this->data['password'],
                'nama_lengkap' => $this->data['nama_lengkap'],
                'email' => $this->data['email'],
                'link' => $link,
            ]);
    }
}
