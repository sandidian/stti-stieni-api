<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PasswordResetRequest extends Notification implements ShouldQueue
{
    use Queueable;

    protected $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url_page = "/smb/reset-password/" . $this->data['token'];
        $url_app = env("URL_FRONTEND_APP_MAIN") . $url_page;
        $from_email = env("EMAIL_FRONTEND_APP_MAIN");
        $scope = 'STTI-STIENI';

        if ($this->data['scope'] != "mahasiswa-baru") {
            $url_page = '/reset-password/' . $this->data['token'];
            $url_app = env("URL_FRONTEND_APP_PRIMAGAMAS") . $url_page;
            $from_email = env("EMAIL_FRONTEND_APP_PRIMAGAMAS");
            $scope = 'STTI-STIENI-PRIMAGAMAS';
        }

        $url = url($url_app);

        // dd([$url_page, $url_app, $from_email, $scope, $this->data]);

        return (new MailMessage)
            ->subject('Reset Password')
            ->from($from_email, $scope)
            ->line('You are receiving this email because we received a password reset request for your account.')
            ->line('Password reset request is only valid for 1 hour.')
            ->action('Reset Password', url($url))
            ->line('If you did not request a password reset, no further action is required.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
