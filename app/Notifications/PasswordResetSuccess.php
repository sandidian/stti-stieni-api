<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PasswordResetSuccess extends Notification implements ShouldQueue
{
    use Queueable;

    protected $scope;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($scope)
    {
        $this->scope = $scope;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url_app = env("URL_FRONTEND_APP_MAIN") . "/smb/login";
        $from_email = env("EMAIL_FRONTEND_APP_MAIN");
        $scope = 'STTI-STIENI';

        if ($this->scope != "mahasiswa-baru") {
            $url_app = env("URL_FRONTEND_APP_PRIMAGAMAS") . "/login";
            $from_email = env("EMAIL_FRONTEND_APP_PRIMAGAMAS");
            $scope = 'STTI-STIENI-PRIMAGAMAS';
        }

        $url = url($url_app);

        return (new MailMessage)
            ->subject('Success Reset Password')
            ->from($from_email, $scope)
            ->line('The introduction to the notification.')
            ->action('Login', url($url))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
