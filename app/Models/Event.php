<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'event';

    public function category()
   	{
		return $this->hasOne(\App\Models\Category::class,'id','id_category');
	}
}
