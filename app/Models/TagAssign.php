<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TagAssign extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'tag_assign';
}
