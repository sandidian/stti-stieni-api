<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Study extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'study';
}
