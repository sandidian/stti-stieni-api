<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'article';

    public function category()
   	{
		return $this->hasOne(\App\Models\Category::class,'id','id_category');
	}
}
