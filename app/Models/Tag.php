<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'tag';
}
