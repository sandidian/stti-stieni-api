<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'gallery';

    public function category()
   	{
		return $this->hasOne(\App\Models\Category::class,'id','id_category');
	}
}
