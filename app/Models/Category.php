<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'category';

    public $params;

    public function __construct()
    {
        $this->params = config('utils');
    }

    public function Article()
    {
        return $this->hasMany(\App\Models\Article::class, 'id_category', 'id')->where(['article.status' => $this->params['status_post']['Publish']]);
    }

    public function Gallery()
    {
        return $this->hasMany(\App\Models\Gallery::class, 'id_category', 'id');
    }

    public function Event()
    {
        return $this->hasMany(\App\Models\Event::class, 'id_category', 'id');
    }

}
