<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Study;
use Illuminate\Http\Request;

class StudyController extends Controller
{
    public function index(Request $request)
    {

        $model = Study::select(['id', 'name', 'content', 'image', 'status'])->where(['status' => 1])->get();

        $response['success'] = true;
        $response['data'] = [
            'study' => $model,
        ];

        return response()->json($response);

    }

}
