<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function index(Request $request)
    {

        $type = $request->type;

        if (!$type) {
            return response()->json([
                'success' => false,
                'message' => 'Query parameter type is required',
            ]);
        }

        $category = Category::where(['type' => $type]);

        if ($type == $this->params['type_category']['article']) {
            $category->withCount('article');
        } elseif ($type == $this->params['type_category']['event']) {
            $category->withCount('event');
        } elseif ($type == $this->params['type_category']['gallery']) {
            $category->withCount('gallery');
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Data not found',
            ]);
        }

        $category = $category->get();

        $response['success'] = true;
        $response['data'] = [
            'category' => $category,
        ];

        return response()->json($response);

    }

    public function detail(Request $request, $id)
    {

        $category = DB::table('category')->where(['id' => $id])->first();

        $response['success'] = true;

        if ($category) {
            $response['data'] = [
                'category' => $category,
            ];
        } else {
            $response['success'] = false;
            $response['message'] = "Data tidak ditemukan";
        }

        return response()->json($response);

    }

}
