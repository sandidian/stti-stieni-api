<?php

namespace App\Http\Controllers\Api\MahasiswaPrimagamas\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\PasswordResetSuccess;
use App\PasswordReset;
use App\UserMahasiswaPrimagamas;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Validator;

class ResetPasswordController extends Controller
{
    public function find(Request $request, $token)
    {
        // return $token;

        $passwordReset = PasswordReset::where('token', $token)
            ->first();

        if (!$passwordReset) {
            return response()->json([
                'success' => false,
                'message' => 'This password reset token is invalid.',
            ]);
        }

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(60)->isPast()) {
            $passwordReset->delete();
            return response()->json([
                'success' => false,
                'message' => 'Password reset token has expired.',
            ]);
        }
        return response()->json([
            'success' => true,
            'data' => $passwordReset,
        ]);
    }

    public function reset(Request $request)
    {

        $rules = [
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed',
            'token' => 'required|string',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['email', $request->email],
        ])->first();

        if (!$passwordReset) {
            return response()->json([
                'success' => false,
                'message' => 'This password reset token is invalid.',
            ]);
        }

        $user = UserMahasiswaPrimagamas::where('email', $passwordReset->email)->first();
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => "We can't find a user with that e-mail address.",
            ]);
        }

        $user->password = Hash::make($request->password);
        $user->save();
        $passwordReset->delete();

        $user->notify(
            new PasswordResetSuccess('primagamas')
        );

        return response()->json([
            'success' => true,
            'message' => 'Successfully changed your password',
        ]);
    }
}
