<?php

namespace App\Http\Controllers\Api\MahasiswaPrimagamas\Auth;

use App\Http\Controllers\Controller;
use App\UserMahasiswaPrimagamas;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class LoginController extends Controller
{
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {

        $email = $request->input('email');
        $password = $request->input('password');

        $rules = [
            'email' => 'required|email|max:255',
            'password' => ['required'],
            'remember_me' => 'boolean',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        if (UserMahasiswaPrimagamas::where('email', $email)->count() <= 0) {
            return response([
                'success' => false,
                "message" => "Email number does not exist",
            ], );
        }

        $checkUser = UserMahasiswaPrimagamas::where(['email' => request()->email])->first();

        if (!Hash::check(request()->password, $checkUser->password)) {
            return response()->json([
                'success' => false,
                'message' => 'Password incorrect',
            ]);
        }

        $user = $checkUser;
        $tokenResult = $user->createToken('Personal Access Token', ['mahasiswa_primagamas']);
        $token = $tokenResult->token;
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }

        $token->save();

        return response()->json([
            'success' => true,
            'token' => $tokenResult->accessToken,
        ]);

    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out',
        ]);
    }

}
