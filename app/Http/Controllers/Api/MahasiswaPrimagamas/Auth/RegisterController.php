<?php

namespace App\Http\Controllers\Api\MahasiswaPrimagamas\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Jobs\SendEmailMahasiswaPrimagamasRegistration;
use App\UserMahasiswaPrimagamas;
use Validator;

class RegisterController extends Controller
{
    public function registerPost(Request $request)
    {
        $rules = [
            'email' => 'required|string|email|unique:users_mahasiswa_primagamas',
            'nama_lengkap' => 'required|string|max:50',
            'phone' => 'required|string|max:30',
            'tempat_lahir' => 'required|string|max:50',
            'tanggal_lahir' => 'required|date',
            'gender' => 'required|integer',
            'alamat' => 'required|string',
            'provinsi_id' => 'required|integer',
            'kota_id' => 'required|integer',
            'kode_pos' => 'required|integer',
            'jurusan' => 'required|json',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first()
            ]);
        }

        $register = DB::transaction(function () use ($request) {

            try
            {
                $password_random = Str::random(10);

                $exp = explode("@",$request->email);

                $username = Str::slug($exp[0], '-');

                $user = new UserMahasiswaPrimagamas;

                $user->username =  $username;
                $user->nama_lengkap = $request->nama_lengkap;
                $user->email = $request->email;
                $user->phone = $request->phone;
                $user->password = Hash::make($password_random);
                $user->tempat_lahir = $request->tempat_lahir;
                $user->tanggal_lahir = $request->tanggal_lahir;
                $user->gender = $request->gender;
                $user->alamat = $request->alamat;
                $user->provinsi_id = $request->provinsi_id;
                $user->kota_id = $request->kota_id;
                $user->kode_pos = $request->kode_pos;
                $user->jurusan = $request->jurusan;
                $user->status = 1;

                $user->save();

                $dispatch = [
                    'nama_lengkap' => $request->nama_lengkap,
                    'email' => $request->email,
                    'password' => $password_random,
                ];
        
                SendEmailMahasiswaPrimagamasRegistration::dispatch($dispatch);

                return response()->json([
                    'success' => true,
                    'message' => 'Successfully created user!'
                ], 201);

            }catch (\Exception $e){
                return $e;  
            }

        });

        return $register;
        
    }
}
