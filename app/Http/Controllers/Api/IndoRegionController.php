<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\Regency;

class IndoRegionController extends Controller
{
    public function province(Request $request)
    {
        $provinces = Province::all();

        if($request->get('id')){
            $provinces = Province::where(['id'=>$request->get('id')])->first();
        }

        return response()->json([
            'success' => true,
            'data' => $provinces,
            
        ]);
    }

    public function regency(Request $request)
    {
        $regency = Regency::with('province')->get();

        if($request->get('province_id')){
            $regency = Regency::where(['province_id'=>$request->get('province_id')])->with('province')->get();
        }

        return response()->json([
            'success' => true,
            'data' => $regency,
            
        ]);
    }
}
