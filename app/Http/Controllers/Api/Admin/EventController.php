<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Validator;

class EventController extends Controller
{
    public $params;

    public function __construct()
    {
        $this->params = config('utils');
    }

    public function index(Request $request)
    {

        $model = Event::select(['id', 'slug', 'title', 'id_category', 'content', 'author', 'image', 'location', 'date_start', 'date_end', 'status', 'published_at'])->with('category')->get();

        $response['success'] = true;
        $response['data'] = [
            'event' => $model,
        ];

        return response()->json($response);

    }

    public function create(Request $request)
    {

        $response['success'] = true;

        $model = new Event;

        $form = $request->all();

        $rules = [
            'title' => 'required|string',
            'id_category' => 'required',
            'location' => 'required',
            'date_start' => 'required',
            'date_end' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg',
        ];

        $validator = Validator::make($form, $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        $user = $request->user();

        $datePath = date("Y") . '/' . date("m") . '/' . date("d");

        $featured_image = $request->file('image');
        $file_name_featured_image = rand() . '.' . $featured_image->getClientOriginalExtension();

        $full_path_featured_image = $datePath . '/' . $file_name_featured_image;
        $featured_image->move(public_path('uploads/event/' . $datePath), $file_name_featured_image);

        $slug = Str::slug($request->title, '-');

        $model->title = $request->title;
        $model->id_category = $request->id_category;
        $model->location = $request->location;
        $model->date_start = $request->date_start;
        $model->date_end = $request->date_end;
        $model->image = $full_path_featured_image;
        $model->slug = $this->incrementSlug($slug);
        $model->status = $this->params['status_post']['Draft'];
        $model->author = $user['id'];

        if ($model->save()) {
            $response['data']['event']['id'] = $model->id;
            $response['message'] = "Data saved successfully";
        } else {
            $response['success'] = false;
            $response['message'] = "Data failed to save";
        }

        return response()->json($response);

    }

    public function edit(Request $request, $id)
    {

        $model = Event::select(['id', 'slug', 'title', 'id_category', 'content', 'author', 'image', 'location', 'date_start', 'date_end', 'status', 'published_at'])->where(['id' => $id])->first();

        $response['success'] = true;

        if ($model) {
            $response['data'] = [
                'event' => $model,
            ];
        } else {
            $response['success'] = false;
            $response['message'] = "Data not found";
        }

        return response()->json($response);

    }

    public function update(Request $request, $id)
    {

        $response['success'] = true;

        $model = Event::where(['id' => $id])->first();

        if (!$model) {
            return response()->json([
                "success" => false,
                "message" => "Data not found",
            ]);
        }

        $form = $request->all();

        // return $form;

        $rules = [
            'title' => 'required|string',
            'id_category' => 'required',
        ];

        if ($this->params['status_post']['Publish'] == $request->status) {
            $rules['content'] = 'required';
            $rules['published_at'] = 'required';
        }

        if ($request->hasFile('image')) {
            $rules['image'] = 'mimes:png,jpg,jpeg';
        }

        $validator = Validator::make($form, $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        $old_image = $model->image;

        if ($request->hasFile('image')) {

            $file = public_path('uploads/event/' . $old_image);

            File::delete($file);

            $datePath = date("Y") . '/' . date("m") . '/' . date("d");

            $featured_image = $request->file('image');
            $file_name_featured_image = rand() . '.' . $featured_image->getClientOriginalExtension();

            $full_path_featured_image = $datePath . '/' . $file_name_featured_image;
            $featured_image->move(public_path('uploads/event/' . $datePath), $file_name_featured_image);

            $model->image = $full_path_featured_image;

        } else {

            $model->image = $old_image;
        }

        if ($request->has('published_at') && $request->published_at !== null && $request->published_at !== "null") {
            $model->published_at = $request->published_at;
        }

        $slug = Str::slug($request->title, '-');

        $model->title = $request->title;
        $model->content = $request->content;
        $model->id_category = $request->id_category;
        $model->location = $request->location;
        $model->date_start = $request->date_start;
        $model->date_end = $request->date_end;
        $model->status = $request->status;
        // $model->slug = $this->incrementSlug($slug);

        if ($model->save()) {
            $response['data']['image'] = $model->image;
            $response['message'] = "Data updated successfully";
        } else {
            $response['success'] = false;
            $response['message'] = "Data failed to update";
        }

        return response()->json($response);

    }

    public function delete(Request $request, $id)
    {

        $response['success'] = true;

        $model = Event::where(['id' => $id])->first();

        if (!$model) {
            return response()->json([
                "success" => false,
                "message" => "Data not found",
            ]);
        }

        $file = public_path('uploads/event/' . $model->image);

        if ($model->delete()) {

            File::delete($file);

            $response['message'] = "Data deleted successfully";
        } else {
            $response['success'] = false;
            $response['message'] = "Data failed to delete";
        }

        return response()->json($response);

    }

    public function incrementSlug($slug)
    {
        $original = $slug;

        $count = 2;

        while (Event::where(['slug' => $slug])->exists()) {

            $slug = "{$original}-" . $count++;
        }

        return $slug;

    }
}
