<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Jobs\SendEmailMahasiswaBaruRegistration;
use App\UserMahasiswaBaru;
use Validator;


class MahasiswaBaruController extends Controller
{
    public function index(Request $request){

        $model = UserMahasiswaBaru::select(['id','nama_lengkap','email','status','created_at'])->orderBy('created_at','DESC')->get();

        $response['success'] = true;
        $response['data'] = [
        	'mahasiswa' => $model
        ];

        return response()->json($response);

    }

    public function create(Request $request){

        $response['success'] = true;

        $rules = [
            'email' => 'required|string|email|unique:users_mahasiswa_baru',
            'nama_lengkap' => 'required|string|max:50',
            'phone' => 'required|string|max:30',
            'tempat_lahir' => 'required|string|max:50',
            'tanggal_lahir' => 'required|date',
            'gender' => 'required|integer',
            'alamat' => 'required|string',
            'provinsi_id' => 'required|integer',
            'kota_id' => 'required|integer',
            'kode_pos' => 'required|integer',
            'pendidikan_terakhir'=> 'required|integer',
            'nama_sekolah' => 'required|string|max:50',
            'alamat_sekolah' => 'required|string',
            'provinsi_id_sekolah' => 'required|integer',
            'kota_id_sekolah' => 'required|integer'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first()
            ]);
        }
                
        $form = $request->all();

        $password_random = Str::random(10);
        $username_random = Str::random(5);

        $exp = explode("@",$request->email);

        $username = Str::slug($exp[0], '-');

        $user = new UserMahasiswaBaru;

        $form['username'] = $username.".".$username_random;
        $form['password'] = Hash::make($password_random);
        $form['status'] = 1;

        foreach ($form as $key => $value) {
            $user->{$key} = $value;
        }

        if($user->save()){

            $dispatch = [
                'nama_lengkap' => $request->nama_lengkap,
                'email' => $request->email,
                'password' => $password_random,
            ];

            SendEmailMahasiswaBaruRegistration::dispatch($dispatch);

            $response['message'] = "Data berhasil disimpan";

        }else{
            $response['success'] = false;
            $response['message'] = "Data gagal disimpan";
        }

        return response()->json($response);
        
    }

    public function edit(Request $request,$id){

        $model = UserMahasiswaBaru::where(['id'=>$id])->first();

        $response['success'] = true;

        if($model){
            $response['data'] = [
                'user' => $model
            ];
        }else{
            $response['success'] = false;
            $response['message'] = "Data tidak ditemukan";
        }

        return response()->json($response);

    }

    public function update(Request $request,$id){

        $response['success'] = true;

        $model = UserMahasiswaBaru::where(['id'=>$id])->first();

        if(!$model){
            return response()->json([
                "success" => false,
                "message" => "Data tidak ditemukan"
            ]);
        }

        $form = $request->all();

        // return $form;

        $rules = [
            'email' => 'required|string|email|unique:users_mahasiswa_baru',
            'nama_lengkap' => 'required|string|max:50',
            'phone' => 'required|string|max:30',
            'tempat_lahir' => 'required|string|max:50',
            'tanggal_lahir' => 'required|date',
            'gender' => 'required|integer',
            'alamat' => 'required|string',
            'provinsi_id' => 'required|integer',
            'kota_id' => 'required|integer',
            'kode_pos' => 'required|integer',
            'pendidikan_terakhir'=> 'required|integer',
            'nama_sekolah' => 'required|string|max:50',
            'alamat_sekolah' => 'required|string',
            'provinsi_id_sekolah' => 'required|integer',
            'kota_id_sekolah' => 'required|integer'
        ];

        if($request->email == $model->email){
			$rules['email'] = 'required|email';
		}else{
			$rules['email'] = 'required|string|email|unique:users';	
		}

        $validator = Validator::make($form, $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first()
            ]);
        }

        foreach ($form as $key => $value) {
            $model->{$key} = $value;
        }

        // return $model;

        if($model->save()){
            $response['message'] = "Data berhasil diupdate";
        }else{
            $response['success'] = false;
            $response['message'] = "Data gagal disimpan";
        }

        return response()->json($response);
        
    }

    public function delete(Request $request,$id){

        $response['success'] = true;

        $model = UserMahasiswaBaru::where(['id'=>$id])->first();

        if(!$model){
            return response()->json([
                "success" => false,
                "message" => "Data tidak ditemukan"
            ]);
        }

        if($model->delete()){
            $response['message'] = "Data berhasil dihapus";
        }else{
            $response['success'] = false;
            $response['message'] = "Data gagal dihapus";
        }

        return response()->json($response);
        
    }
}
