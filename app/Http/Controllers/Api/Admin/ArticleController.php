<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\TagAssign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Validator;

class ArticleController extends Controller
{
    public $params;

    public function __construct()
    {
        $this->params = config('utils');
    }

    public function index(Request $request)
    {

        $model = Article::select(['id', 'slug', 'title', 'id_category', 'content', 'author', 'image', 'status', 'viewer', 'published_at'])->with('category')->get();

        $response['success'] = true;
        $response['data'] = [
            'article' => $model,
        ];

        return response()->json($response);

    }

    public function create(Request $request)
    {

        $response['success'] = true;

        $model = new Article;

        $form = $request->all();

        // return $form;

        $rules = [
            'title' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg',
            'id_category' => 'required',
        ];

        $validator = Validator::make($form, $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        $user = $request->user();

        $datePath = date("Y") . '/' . date("m") . '/' . date("d");

        $featured_image = $request->file('image');
        $file_name_featured_image = rand() . '.' . $featured_image->getClientOriginalExtension();

        $full_path_featured_image = $datePath . '/' . $file_name_featured_image;
        $featured_image->move(public_path('uploads/article/' . $datePath), $file_name_featured_image);

        $slug = Str::slug($request->title, '-');

        $model->title = $request->title;
        $model->id_category = $request->id_category;
        $model->image = $full_path_featured_image;
        $model->slug = $this->incrementSlug($slug);
        $model->status = $this->params['status_post']['Draft'];
        $model->author = $user['id'];

        if ($model->save()) {

            if ($request->has('tag') && $request->tag !== null && $request->tag !== "null") {

                $this->insetTag($request->tag, $model->id);

            }

            $response['data']['article']['id'] = $model->id;
            $response['message'] = "Data saved successfully";
        } else {
            $response['success'] = false;
            $response['message'] = "Data failed to save";
        }

        return response()->json($response);

    }

    public function edit(Request $request, $id)
    {

        $model = Article::select(['id', 'slug', 'title', 'id_category', 'tag', 'content', 'author', 'image', 'status', 'viewer', 'published_at'])->where(['id' => $id])->first();

        $tag_assign = TagAssign::select(['id_tag', 'id_post'])->where(['id_post' => $id, 'type' => $this->params['type_category']['article']])->get();

        $response['success'] = true;

        if ($model) {
            $response['data'] = [
                'article' => $model,
                'tag' => $tag_assign,
            ];
        } else {
            $response['success'] = false;
            $response['message'] = "Data not found";
        }

        return response()->json($response);

    }

    public function update(Request $request, $id)
    {

        $response['success'] = true;

        $model = Article::where(['id' => $id])->first();

        if (!$model) {
            return response()->json([
                "success" => false,
                "message" => "Data not found",
            ]);
        }

        $form = $request->all();

        // return $form;

        $rules = [
            'title' => 'required',
            'id_category' => 'required',
        ];

        if ($this->params['status_post']['Publish'] == $request->status) {
            $rules['content'] = 'required';
            $rules['published_at'] = 'required';
        }

        if ($request->hasFile('image')) {
            $rules['image'] = 'mimes:png,jpg,jpeg';
        }

        $validator = Validator::make($form, $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        $old_image = $model->image;

        if ($request->hasFile('image')) {

            $file = public_path('uploads/article/' . $old_image);

            File::delete($file);

            $datePath = date("Y") . '/' . date("m") . '/' . date("d");

            $featured_image = $request->file('image');
            $file_name_featured_image = rand() . '.' . $featured_image->getClientOriginalExtension();

            $full_path_featured_image = $datePath . '/' . $file_name_featured_image;
            $featured_image->move(public_path('uploads/article/' . $datePath), $file_name_featured_image);

            $model->image = $full_path_featured_image;

        } else {

            $model->image = $old_image;
        }

        if ($request->has('published_at') && $request->published_at !== null && $request->published_at !== "null") {
            $model->published_at = $request->published_at;
        }

        $slug = Str::slug($request->title, '-');

        $model->title = $request->title;
        $model->status = $request->status;
        $model->content = $request->content;
        $model->id_category = $request->id_category;
        // $model->slug = $this->incrementSlug($slug);

        if ($model->save()) {

            DB::table('tag_assign')->where(['id_post' => $model->id, 'type' => $this->params['type_category']['article']])->delete();

            if ($request->has('tag') && $request->tag !== null && $request->tag !== "null") {

                $this->insetTag($request->tag, $model->id);

            }

            $response['data']['image'] = $model->image;
            $response['message'] = "Data updated successfully";
        } else {
            $response['success'] = false;
            $response['message'] = "Data failed to update";
        }

        return response()->json($response);

    }

    public function delete(Request $request, $id)
    {

        $response['success'] = true;

        $model = Article::where(['id' => $id])->first();

        if (!$model) {
            return response()->json([
                "success" => false,
                "message" => "Data not found",
            ]);
        }

        $file = public_path('uploads/article/' . $model->image);

        if ($model->delete()) {

            File::delete($file);

            DB::table('tag_assign')->where(['id_post' => $id, 'type' => $this->params['type_category']['article']])->delete();

            $response['message'] = "Data deleted successfully";
        } else {
            $response['success'] = false;
            $response['message'] = "Data failed to delete";
        }

        return response()->json($response);

    }

    public function insetTag($data, $id_post)
    {
        foreach (json_decode($data) as $key => $value) {

            DB::table('tag_assign')->insert([
                'id_post' => $id_post,
                'id_tag' => $value,
                'type' => $this->params['type_category']['article'],
            ]);
        }
    }

    public function incrementSlug($slug)
    {
        $original = $slug;

        $count = 2;

        while (Article::where(['slug' => $slug])->exists()) {

            $slug = "{$original}-" . $count++;
        }

        return $slug;

    }
}
