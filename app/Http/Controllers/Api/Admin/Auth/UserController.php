<?php

namespace App\Http\Controllers\Api\Admin\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;

class UserController extends Controller
{
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */

    public function index(Request $request)
    {
        return response()->json([
            'success' => true,
            'data' => $request->user(),
        ]);
    }

    public function changePassword(Request $request)
    {
        $user = User::findOrFail($request->id);

        // return $request

        $rules = [
            'password' => 'required',
            'new_password' => 'min:8|different:password',
        ];

        $customMessages = [
            'different' => 'The new password and old password must be different.',
        ];

        $validator = Validator::make($request->all(), $rules, $customMessages);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        if (Hash::check($request->password, $user->password)) {
            $user->fill([
                'password' => Hash::make($request->new_password),
            ])->save();

            return response()->json([
                'success' => true,
                'data' => $request->user(),
                'message' => 'Password changed',
            ]);

        } else {
            return response()->json([
                'success' => false,
                'message' => 'Old Password does not match',
            ]);
        }

    }

}
