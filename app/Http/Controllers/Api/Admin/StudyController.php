<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Study;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Validator;

class StudyController extends Controller
{
    public function index(Request $request)
    {

        $model = Study::select(['id', 'name', 'content', 'image', 'status'])->get();

        $response['success'] = true;
        $response['data'] = [
            'study' => $model,
        ];

        return response()->json($response);

    }

    public function create(Request $request)
    {

        $response['success'] = true;

        $model = new Study;

        $form = $request->all();

        $rules = [
            'name' => 'required|string',
            'content' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg',
        ];

        $validator = Validator::make($form, $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        $datePath = date("Y") . '/' . date("m") . '/' . date("d");

        $featured_image = $request->file('image');
        $file_name_featured_image = rand() . '.' . $featured_image->getClientOriginalExtension();

        $full_path_featured_image = $datePath . '/' . $file_name_featured_image;
        $featured_image->move(public_path('uploads/study/' . $datePath), $file_name_featured_image);

        $model->name = $request->name;
        $model->content = $request->content;
        $model->image = $full_path_featured_image;

        if ($model->save()) {
            $response['message'] = "Data saved successfully";
        } else {
            $response['success'] = false;
            $response['message'] = "Data failed to save";
        }

        return response()->json($response);

    }

    public function edit(Request $request, $id)
    {

        $model = Study::select(['id', 'name', 'content', 'image', 'status'])->where(['id' => $id])->first();

        $response['success'] = true;

        if ($model) {
            $response['data'] = [
                'study' => $model,
            ];
        } else {
            $response['success'] = false;
            $response['message'] = "Data not found";
        }

        return response()->json($response);

    }

    public function update(Request $request, $id)
    {

        $response['success'] = true;

        $model = Study::where(['id' => $id])->first();

        if (!$model) {
            return response()->json([
                "success" => false,
                "message" => "Data not found",
            ]);
        }

        $form = $request->all();

        $rules = [
            'name' => 'required|string',
            'content' => 'required',
        ];

        if ($request->hasFile('image')) {
            $rules['image'] = 'mimes:png,jpg,jpeg';
        }

        $validator = Validator::make($form, $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        $old_image = $model->image;

        if ($request->hasFile('image')) {

            $file = public_path('uploads/study/' . $old_image);

            File::delete($file);

            $datePath = date("Y") . '/' . date("m") . '/' . date("d");

            $featured_image = $request->file('image');
            $file_name_featured_image = rand() . '.' . $featured_image->getClientOriginalExtension();

            $full_path_featured_image = $datePath . '/' . $file_name_featured_image;
            $featured_image->move(public_path('uploads/study/' . $datePath), $file_name_featured_image);

            $model->image = $full_path_featured_image;

        } else {

            $model->image = $old_image;
        }

        $model->name = $request->name;
        $model->content = $request->content;
        // $model->status = $request->status;

        if ($model->save()) {
            $response['data']['image'] = $model->image;
            $response['message'] = "Data updated successfully";
        } else {
            $response['success'] = false;
            $response['message'] = "Data failed to update";
        }

        return response()->json($response);

    }

    public function delete(Request $request, $id)
    {

        $response['success'] = true;

        $model = Study::where(['id' => $id])->first();

        if (!$model) {
            return response()->json([
                "success" => false,
                "message" => "Data not found",
            ]);
        }

        if ($model->delete()) {
            $response['message'] = "Data deleted successfully";
        } else {
            $response['success'] = false;
            $response['message'] = "Data failed to delete";
        }

        return response()->json($response);

    }
}
