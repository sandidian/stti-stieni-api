<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;

class UserController extends Controller
{
    public function index(Request $request)
    {

        $model = User::select(['id', 'name', 'email', 'foto', 'status'])->get();

        $response['success'] = true;
        $response['data'] = [
            'users' => $model,
        ];

        return response()->json($response);

    }

    public function create(Request $request)
    {

        $response['success'] = true;

        $model = new User;

        $form = $request->all();

        $rules = [
            'name' => 'required|string|max:50',
            'email' => 'required|string|email|unique:users',
            'password' => 'required',
        ];

        $validator = Validator::make($form, $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        $form['status'] = 1;
        $form['foto'] = 'fotos/profile/user-uploads/user-01.jpg';
        $form['password'] = Hash::make($form['password']);

        foreach ($form as $key => $value) {
            $model->{$key} = $value;
        }

        if ($model->save()) {
            $response['message'] = "Data saved successfully";
        } else {
            $response['success'] = false;
            $response['message'] = "Data failed to save";
        }

        return response()->json($response);

    }

    public function edit(Request $request, $id)
    {

        $model = User::select(['id', 'name', 'email', 'foto', 'status'])->where(['id' => $id])->first();

        $response['success'] = true;

        if ($model) {
            $response['data'] = [
                'user' => $model,
            ];
        } else {
            $response['success'] = false;
            $response['message'] = "Data not found";
        }

        return response()->json($response);

    }

    public function update(Request $request, $id)
    {

        $response['success'] = true;

        $model = User::where(['id' => $id])->first();

        if (!$model) {
            return response()->json([
                "success" => false,
                "message" => "Data not found",
            ]);
        }

        $form = $request->all();

        $rules = [
            'name' => 'required|string|max:50',
            'status' => 'required',
        ];

        if ($request->email == $model->email) {
            $rules['email'] = 'required|email';
        } else {
            $rules['email'] = 'required|string|email|unique:users';
        }

        $validator = Validator::make($form, $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        $old_foto = $model->foto;

        if ($request->hasFile('foto')) {

            $dir = 'images/profile/user-uploads/';

            $file = public_path($dir . $old_foto);

            // File::delete($file);

            $featured_foto = $request->file('foto');
            $file_name_featured_foto = rand() . '.' . $featured_foto->getClientOriginalExtension();

            $featured_foto->move(public_path($dir), $file_name_featured_foto);

            $model->foto = $dir . $file_name_featured_foto;

        } else {

            $model->foto = $old_foto;
        }

        // return $model;

        $model->name = $request->name;
        $model->email = $request->email;
        $model->status = $request->status;

        if ($model->save()) {
            $response['data']['foto'] = $model->foto;
            $response['message'] = "Data updated successfully";
        } else {
            $response['success'] = false;
            $response['message'] = "Data failed to update";
        }

        return response()->json($response);

    }

    public function delete(Request $request, $id)
    {

        $response['success'] = true;

        $model = User::where(['id' => $id])->first();

        if (!$model) {
            return response()->json([
                "success" => false,
                "message" => "Data not found",
            ]);
        }

        if ($model->delete()) {
            $response['message'] = "Data deleted successfully";
        } else {
            $response['success'] = false;
            $response['message'] = "Data failed to delete";
        }

        return response()->json($response);

    }
}
