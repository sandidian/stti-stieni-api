<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;

class TagArticleController extends Controller
{
    public $type;

    public function __construct()
    {
        $this->type = config('utils.type_tag')['article'];
    }

    public function index(Request $request)
    {

        $model = Tag::select(['id', 'name', 'slug', 'type', 'status'])->where(['type' => $this->type])->get();

        $response['success'] = true;
        $response['data'] = [
            'tag' => $model,
        ];

        return response()->json($response);

    }

    public function create(Request $request)
    {

        $response['success'] = true;

        $model = new Tag;

        $form = $request->all();

        $rules = [
            'name' => 'required|string|max:50',
        ];

        $validator = Validator::make($form, $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        $slug = Str::slug($request->name, '-');

        $model->name = $request->name;
        $model->type = $this->type;
        $model->slug = $this->incrementSlug($slug);

        if ($model->save()) {
            $response['message'] = "Data berhasil disimpan";
        } else {
            $response['success'] = false;
            $response['message'] = "Data gagal disimpan";
        }

        return response()->json($response);

    }

    public function edit(Request $request, $id)
    {
        $model = Tag::select(['id', 'name', 'slug', 'type', 'status'])->where(['type' => $this->type, 'id' => $id])->first();

        $response['success'] = true;

        if ($model) {
            $response['data'] = [
                'tag' => $model,
            ];
        } else {
            $response['success'] = false;
            $response['message'] = "Data tidak ditemukan";
        }

        return response()->json($response);

    }

    public function update(Request $request, $id)
    {

        $response['success'] = true;

        $model = Tag::where(['type' => $this->type, 'id' => $id])->first();

        if (!$model) {
            return response()->json([
                "success" => false,
                "message" => "Data tidak ditemukan",
            ]);
        }

        $form = $request->all();

        $rules = [
            'name' => 'required|string|max:50',
        ];

        $validator = Validator::make($form, $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        $slug = Str::slug($request->name, '-');

        $model->name = $request->name;
        $model->slug = $this->incrementSlug($slug);

        if ($model->save()) {
            $response['message'] = "Data berhasil diupdate";
        } else {
            $response['success'] = false;
            $response['message'] = "Data gagal disimpan";
        }

        return response()->json($response);

    }

    public function delete(Request $request, $id)
    {

        $response['success'] = true;

        $model = Tag::where(['type' => $this->type, 'id' => $id])->first();

        if (!$model) {
            return response()->json([
                "success" => false,
                "message" => "Data tidak ditemukan",
            ]);
        }

        if ($model->delete()) {
            $response['message'] = "Data berhasil dihapus";
        } else {
            $response['success'] = false;
            $response['message'] = "Data gagal dihapus";
        }

        return response()->json($response);

    }

    public function incrementSlug($slug)
    {

        $original = $slug;

        $count = 2;

        while (Tag::where(['slug' => $slug, 'type' => $this->type])->exists()) {

            $slug = "{$original}-" . $count++;
        }

        return $slug;

    }
}
