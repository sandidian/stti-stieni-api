<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Validator;

class GalleryController extends Controller
{

    public function index(Request $request)
    {

        $category = Category::where(['type' => $this->params['type_category']['gallery']])->withCount('gallery')->get();
        $gallery = Gallery::select(['id', 'title', 'id_category', 'content', 'image', 'status', 'created_at', 'updated_at'])->with('category')->get();

        $response['success'] = true;
        $response['data'] = [
            'gallery' => $gallery,
            'category' => $category,
        ];

        return response()->json($response);

    }

    public function create(Request $request)
    {

        $response['success'] = true;

        $model = new Gallery;

        $form = $request->all();

        // return $form;

        $rules = [
            'title' => 'required|string|max:50',
            'image' => 'required|mimes:png,jpg,jpeg',
            'content' => 'required',
            'id_category' => 'required',
        ];

        $validator = Validator::make($form, $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        $datePath = date("Y") . '/' . date("m") . '/' . date("d");

        $featured_image = $request->file('image');
        $file_name_featured_image = rand() . '.' . $featured_image->getClientOriginalExtension();

        $full_path_featured_image = $datePath . '/' . $file_name_featured_image;
        $featured_image->move(public_path('uploads/gallery/' . $datePath), $file_name_featured_image);

        $model->title = $request->title;
        $model->content = $request->content;
        $model->id_category = $request->id_category;
        $model->image = $full_path_featured_image;

        if ($model->save()) {
            $response['message'] = "Data berhasil disimpan";
        } else {
            $response['success'] = false;
            $response['message'] = "Data gagal disimpan";
        }

        return response()->json($response);

    }

    public function edit(Request $request, $id)
    {

        $model = Gallery::select(['id', 'title', 'id_category', 'content', 'image', 'status', 'created_at', 'updated_at'])->where(['id' => $id])->orderBy('created_at', 'DESC')->first();

        $response['success'] = true;

        if ($model) {
            $response['data'] = [
                'gallery' => $model,
            ];
        } else {
            $response['success'] = false;
            $response['message'] = "Data tidak ditemukan";
        }

        return response()->json($response);

    }

    public function update(Request $request, $id)
    {

        $response['success'] = true;

        $model = Gallery::where(['id' => $id])->first();

        if (!$model) {
            return response()->json([
                "success" => false,
                "message" => "Data tidak ditemukan",
            ]);
        }

        $form = $request->all();

        $rules = [
            'title' => 'required|string|max:50',
            'content' => 'required',
            'id_category' => 'required',
        ];

        if ($request->hasFile('image')) {
            $rules['image'] = 'mimes:png,jpg,jpeg';
        }

        $validator = Validator::make($form, $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        $old_image = $model->image;

        if ($request->hasFile('image')) {

            $file = public_path('uploads/gallery/' . $old_image);

            File::delete($file);

            $datePath = date("Y") . '/' . date("m") . '/' . date("d");

            $featured_image = $request->file('image');
            $file_name_featured_image = rand() . '.' . $featured_image->getClientOriginalExtension();

            $full_path_featured_image = $datePath . '/' . $file_name_featured_image;
            $featured_image->move(public_path('uploads/gallery/' . $datePath), $file_name_featured_image);

            $model->image = $full_path_featured_image;

        } else {

            $model->image = $old_image;
        }

        $model->title = $request->title;
        $model->status = $request->status;
        $model->content = $request->content;
        $model->id_category = $request->id_category;

        if ($model->save()) {
            $response['data']['image'] = $model->image;
            $response['message'] = "Data berhasil diupdate";
        } else {
            $response['success'] = false;
            $response['message'] = "Data gagal disimpan";
        }

        return response()->json($response);

    }

    public function delete(Request $request, $id)
    {

        $response['success'] = true;

        $model = Gallery::where(['id' => $id])->first();

        if (!$model) {
            return response()->json([
                "success" => false,
                "message" => "Data tidak ditemukan",
            ]);
        }

        $file = public_path('uploads/gallery/' . $model->image);

        if ($model->delete()) {

            File::delete($file);

            $response['message'] = "Data berhasil dihapus";
        } else {
            $response['success'] = false;
            $response['message'] = "Data gagal dihapus";
        }

        return response()->json($response);

    }

}
