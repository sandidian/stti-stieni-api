<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{

    public function index(Request $request)
    {
        $services = DB::table('article')
            ->where(['category.type' => $this->params['type_category']['article']])
            ->select('article.*', 'category.name as category_name', 'category.slug as category_slug', )
            ->join('category', 'article.id_category', '=', 'category.id');

        $services->where(['article.status' => $this->params['status_post']['Publish']]);

        $queryStringArray = [];

        if ($request->search) {

            $services->where('article.title', 'LIKE', '%' . $request->search . '%');

            $queryStringArray['search'] = $request->search;
        }

        if ($request->category) {

            $services->where('article.id_category', $request->category);

            $queryStringArray['category'] = $request->category;
        }

        $category = [];

        if ($request->slug_category) {

            $services->where('category.slug', $request->slug_category);

            $queryStringArray['slug_category'] = $request->slug_category;

            $category = DB::table('category')
                ->where(['type' => $this->params['type_category']['article'], 'slug' => $request->slug_category])->first();
        }

        $tag = [];

        if ($request->slug_tag) {

            $queryStringArray['slug_tag'] = $request->slug_tag;

            $tag = DB::table('tag')->where(['type' => $this->params['type_category']['article'], 'slug' => $request->slug_tag])->first();

            if ($tag) {

                $services->join('tag_assign', 'article.id', '=', 'tag_assign.id_post');
                $services->join('tag', 'tag.id', '=', 'tag_assign.id_tag');
                $services->select('article.*', 'category.name as category_name', 'category.slug as category_slug', 'tag.name as tag_name');
                $services->where(['tag_assign.id_tag' => $tag->id]);
            }

        }

        if ($request->without_id) {
            $services->whereNotIn('article.id', json_decode($request->without_id));
        }

        if ($request->type == "popular") {
            $services->orderBy('article.viewer', 'DESC');
        } else {
            $services->orderBy('article.created_at', 'DESC');
        }

        if ($request->pagination == "false") {

            if ($request->limit) {
                $services->limit($request->limit);
            }

            $article = $services->get();
        } else {
            $article = $services->paginate(9);
            $article->appends($queryStringArray);
        }

        $response['success'] = true;
        $response['data'] = [
            'article' => $article,
            'category' => $category,
            'tag' => $tag,
        ];

        return response()->json($response);

    }

    public function detail(Request $request, $slug)
    {
        // return $this->params['type_category']['article'];

        $article = DB::table('article')
            ->where(['category.type' => $this->params['type_category']['article'], 'article.slug' => $slug])
            ->select('article.*', 'category.name as category_name', 'category.slug as category_slug')
            ->join('category', 'article.id_category', '=', 'category.id')
            ->first();

        $tag_assign = $services = DB::table('tag_assign')
            ->select(['tag.*'])
            ->join('tag', 'tag.id', '=', 'tag_assign.id_tag')
            ->where(['tag_assign.id_post' => $article->id, 'tag_assign.type' => $this->params['type_category']['article']])
            ->get();

        $response['success'] = true;

        if ($article) {
            $response['data'] = [
                'article' => $article,
                'tag' => $tag_assign,
            ];
        } else {
            $response['success'] = false;
            $response['message'] = "Data tidak ditemukan";
        }

        return response()->json($response);

    }

    public function addViewer($slug)
    {
        $article = Article::where(['slug' => $slug])->first();

        $response['success'] = true;

        if ($article) {

            $total = $article->viewer + 1;

            $article->viewer = $total;
            $article->save();

            $response['data'] = $article->viewer;
            $response['message'] = "Successful adding viewer";
        } else {
            $response['success'] = false;
            $response['message'] = "Data not found";
        }

        return response()->json($response);
    }

}
