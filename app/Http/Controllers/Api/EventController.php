<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    public $type_category;

    public function __construct()
    {
        $this->type_category = config('utils.type_category')['event'];
    }

    public function index(Request $request)
    {

        $services = DB::table('event')
            ->where(['category.type' => $this->type_category])
            ->select('event.*', 'category.name as category_name', 'category.slug as category_slug', )
            ->join('category', 'event.id_category', '=', 'category.id');

        $queryStringArray = [];

        $now = Carbon::now()->format('Y-m-d H:i:s');

        $services->where('event.published_at', '<', $now);
        $services->orderBy('event.published_at', 'DESC');

        if ($request->search) {

            $services->where('event.title', 'LIKE', '%' . $request->search . '%');

            $queryStringArray['search'] = $request->search;
        }

        if ($request->category) {

            $services->where('event.id_category', $request->category);

            $queryStringArray['category'] = $request->category;
        }

        if ($request->pagination == "false") {

            if ($request->limit) {
                $services->limit($request->limit);
            }

            $event = $services->get();
        } else {
            $event = $services->paginate(9);
            $event->appends($queryStringArray);
        }

        $response['success'] = true;
        $response['data'] = [
            'event' => $event,
        ];

        return response()->json($response);

    }

    public function detail(Request $request, $slug)
    {

        $event = DB::table('event')
            ->where(['category.type' => $this->type_category])
            ->select('event.*', 'category.name as category_name', 'category.slug as category_slug', )
            ->join('category', 'event.id_category', '=', 'category.id')
            ->where(['event.slug' => $slug])
            ->first();

        $response['success'] = true;

        if ($event) {
            $response['data'] = [
                'event' => $event,
            ];
        } else {
            $response['success'] = false;
            $response['message'] = "Data tidak ditemukan";
        }

        return response()->json($response);

    }

}
