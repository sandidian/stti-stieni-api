<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\PasswordResetRequest;
use App\PasswordReset;
use App\UserMahasiswaBaru;
use App\UserMahasiswaPrimagamas;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;

class ForgotPasswordController extends Controller
{
    public function sendResetPassword(Request $request)
    {
        $rules = [
            'email' => 'required|string|email',
            'scope' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        $scope = $request->scope;

        if ($scope == "mahasiswa-baru") {
            $user = UserMahasiswaBaru::where('email', $request->email)->first();
        } elseif ($scope == "mahasiswa-primagamas") {
            $user = UserMahasiswaPrimagamas::where('email', $request->email)->first();
        }

        // return $scope;

        if (!$user) {
            return response()->json([
                "success" => false,
                'message' => "Kami tidak dapat menemukan pengguna dengan alamat email tersebut.",
            ]);
        }

        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email, 'scope' => $scope],
            [
                'scope' => $scope,
                'email' => $user->email,
                'token' => Str::random(60),
            ]
        );

        if ($user && $passwordReset) {

            $data['token'] = $passwordReset->token;
            $data['scope'] = $scope;

            $user->notify(
                new PasswordResetRequest($data)
            );
        }

        return response()->json([
            'success' => true,
            'message' => 'Cek email Anda untuk mengganti password.',
        ]);

    }
}
