<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\PasswordResetSuccess;
use App\PasswordReset;
use App\UserMahasiswaBaru;
use App\UserMahasiswaPrimagamas;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Validator;

class ResetPasswordController extends Controller
{
    public function find(Request $request, $scope, $token)
    {

        $passwordReset = PasswordReset::where(['token' => $token, 'scope' => $scope])
            ->first();

        if (!$passwordReset) {
            return response()->json([
                'success' => false,
                'message' => 'This password reset token is invalid.',
            ]);
        }

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(60)->isPast()) {
            $passwordReset->delete();
            return response()->json([
                'success' => false,
                'message' => 'Password reset token has expired.',
            ]);
        }
        return response()->json([
            'success' => true,
            'data' => $passwordReset,
        ]);
    }

    public function reset(Request $request)
    {
        $rules = [
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed',
            'token' => 'required|string',
            'scope' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        $scope = $request->scope;

        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['email', $request->email],
            ['scope', $request->scope],
        ])->first();

        if (!$passwordReset) {
            return response()->json([
                'success' => false,
                'message' => 'This password reset token is invalid.',
            ]);
        }

        if ($scope == "mahasiswa-baru") {
            $user = UserMahasiswaBaru::where('email', $passwordReset->email)->first();
        } elseif ($scope == "mahasiswa-primagamas") {
            $user = UserMahasiswaPrimagamas::where('email', $passwordReset->email)->first();
        }

        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => "Kami tidak dapat menemukan pengguna dengan alamat email tersebut.",
            ]);
        }

        $user->password = Hash::make($request->password);
        $user->save();
        $passwordReset->delete();

        $user->notify(
            new PasswordResetSuccess($scope)
        );

        return response()->json([
            'success' => true,
            'message' => 'Berhasil mengubah password Anda.',
        ]);
    }
}
