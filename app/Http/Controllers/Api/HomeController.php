<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $version = app()->version();

        return "Laravel (" . $version . ") (Laravel Components " . $version . ")";
    }

    public function unauthorized()
    {
        return response()->json("unauthorized", 401);
    }
}
