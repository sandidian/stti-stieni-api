<?php

namespace App\Http\Controllers\Api\MahasiswaBaru\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Jobs\SendEmailMahasiswaBaruRegistration;
use App\UserMahasiswaBaru;
use Validator;

class RegisterController extends Controller
{
    public function registerPost(Request $request)
    {
        $rules = [
            'email' => 'required|string|email|unique:users_mahasiswa_baru',
            'nama_lengkap' => 'required|string|max:50',
            'phone' => 'required|string|max:30',
            'tempat_lahir' => 'required|string|max:50',
            'tanggal_lahir' => 'required|date',
            'gender' => 'required|integer',
            'alamat' => 'required|string',
            'provinsi_id' => 'required|integer',
            'kota_id' => 'required|integer',
            'kode_pos' => 'required|integer',
            'pendidikan_terakhir'=> 'required|integer',
            'nama_sekolah' => 'required|string|max:50',
            'alamat_sekolah' => 'required|string',
            'provinsi_id_sekolah' => 'required|integer',
            'kota_id_sekolah' => 'required|integer'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first()
            ]);
        }

        $register = DB::transaction(function () use ($request) {

            try
            {
                
                $form = $request->all();

                $password_random = Str::random(10);

                $exp = explode("@",$request->email);

                $username = Str::slug($exp[0], '-');

                $user = new UserMahasiswaBaru;

                $form['username'] = $username;
                $form['password'] = Hash::make($password_random);
                $form['status'] = 1;

                foreach ($form as $key => $value) {
                    $user->{$key} = $value;
                }

                $user->save();

                $dispatch = [
                    'nama_lengkap' => $request->nama_lengkap,
                    'email' => $request->email,
                    'password' => $password_random,
                ];
        
                SendEmailMahasiswaBaruRegistration::dispatch($dispatch);

                return response()->json([
                    'success' => true,
                    'message' => 'Successfully created user!'
                ], 201);

            }catch (\Exception $e){
                return $e;  
            }

        });

        return $register;
        
    }
}
