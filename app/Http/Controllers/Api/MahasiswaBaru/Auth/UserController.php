<?php

namespace App\Http\Controllers\Api\MahasiswaBaru\Auth;

use App\Http\Controllers\Controller;
use App\UserMahasiswaBaru;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;

class UserController extends Controller
{
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */

    public function index(Request $request)
    {
        return response()->json([
            'success' => true,
            'data' => $request->user(),
        ]);
    }

    public function changePassword(Request $request)
    {
        $user = UserMahasiswaBaru::findOrFail($request->id);

        // return $request

        $rules = [
            'password' => 'required',
            'new_password' => 'min:8|different:password',
        ];

        $customMessages = [
            'different' => 'The new password and old password must be different.',
        ];

        $validator = Validator::make($request->all(), $rules, $customMessages);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        if (Hash::check($request->password, $user->password)) {
            $user->fill([
                'password' => Hash::make($request->new_password),
            ])->save();

            return response()->json([
                'success' => true,
                'data' => $request->user(),
                'message' => 'Password changed',
            ]);

        } else {
            return response()->json([
                'success' => false,
                'message' => 'Old Password does not match',
            ]);
        }

    }

    public function updateProfile(Request $request)
    {
        // return response()->json($request->user());

        $user = $request->user();

        $response['success'] = true;

        $model = UserMahasiswaBaru::where(['id' => $user['id']])->first();

        if (!$model) {
            return response()->json([
                "success" => false,
                "message" => "Data tidak ditemukan",
            ]);
        }

        $form = $request->all();

        // return $form;

        $rules = [
            'email' => 'required|string|email|unique:users_mahasiswa_baru',
            'nama_lengkap' => 'required|string|max:50',
            'phone' => 'required|string|max:30',
            'tempat_lahir' => 'required|string|max:50',
            'tanggal_lahir' => 'required|date',
            'gender' => 'required|integer',
            'alamat' => 'required|string',
            'provinsi_id' => 'required|integer',
            'kota_id' => 'required|integer',
            'kode_pos' => 'required|integer',
            'pendidikan_terakhir' => 'required|integer',
            'nama_sekolah' => 'required|string|max:50',
            'alamat_sekolah' => 'required|string',
            'provinsi_id_sekolah' => 'required|integer',
            'kota_id_sekolah' => 'required|integer',
        ];

        if ($request->email == $model->email) {
            $rules['email'] = 'required|email';
        } else {
            $rules['email'] = 'required|string|email|unique:users';
        }

        $validator = Validator::make($form, $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        foreach ($form as $key => $value) {
            $model->{$key} = $value;
        }

        // return $model;

        if ($model->save()) {
            $response['data'] = $model;
            $response['message'] = "Data berhasil diupdate";
        } else {
            $response['success'] = false;
            $response['message'] = "Data gagal disimpan";
        }

        return response()->json($response);

    }

}
