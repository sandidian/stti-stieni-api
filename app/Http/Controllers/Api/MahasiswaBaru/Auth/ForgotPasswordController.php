<?php

namespace App\Http\Controllers\Api\MahasiswaBaru\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\PasswordResetRequest;
use App\PasswordReset;
use App\UserMahasiswaBaru;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;

class ForgotPasswordController extends Controller
{
    public function sendResetPassword(Request $request)
    {
        $rules = [
            'email' => 'required|string|email',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first(),
            ]);
        }

        $user = UserMahasiswaBaru::where('email', $request->email)->first();

        // return $user;

        if (!$user) {
            return response()->json([
                "success" => false,
                'message' => "We can't find a user with that e-mail address.",
            ]);
        }

        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => Str::random(60),
            ]
        );

        if ($user && $passwordReset) {

            $data['token'] = $passwordReset->token;
            $data['scope'] = 'default';

            $user->notify(
                new PasswordResetRequest($data)
            );
        }

        return response()->json([
            'success' => true,
            'message' => 'We have e-mailed your password reset link!',
        ]);

    }
}
