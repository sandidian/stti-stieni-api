<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    
    public function profile(){
      $breadcrumbs = [
          ['link'=>"/",'name'=>"Home"], ['link'=>"/user/index",'name'=>"Users"], ['name'=>"Profile"]
      ];

      $data = Auth::user();

      return view('/pages/user/profile', [
          'data' => $data,
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    public function profilePost(Request $request){
        
        $data = Auth::user();

        $data->name = $request->name;
        $data->email = $request->email;
        $data->save();

        return back();
    
    }

}
