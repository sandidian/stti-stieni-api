<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class UserMahasiswaPrimagamas extends Authenticatable
{
    use HasApiTokens, Notifiable,HasRoles;

    protected $table = 'users_mahasiswa_primagamas';

    protected $fillable = [
        'nama_lengkap', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
