<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/', 'Api\HomeController@index');

Route::get('/unauthorized', 'Api\UserController@unauthorized');

Route::group([
    'prefix' => 'v1',
], function () {

    Route::get('province', 'Api\IndoRegionController@province');
    Route::get('regency', 'Api\IndoRegionController@regency');

    Route::get('gallery', 'Api\GalleryController@index');

    Route::group([
        'prefix' => 'category',
    ], function () {
        Route::get('/', 'Api\CategoryController@index');
        Route::get('/detail/{slug}', 'Api\CategoryController@detail');
    });

    Route::group([
        'prefix' => 'article',
    ], function () {
        Route::get('/', 'Api\ArticleController@index');
        Route::get('/detail/{slug}', 'Api\ArticleController@detail');
        Route::post('/add-viewer/{slug}', 'Api\ArticleController@addViewer');
    });

    Route::group([
        'prefix' => 'event',
    ], function () {
        Route::get('/', 'Api\EventController@index');
        Route::get('/detail/{slug}', 'Api\EventController@detail');
    });

    Route::group([
        'prefix' => 'auth',
    ], function () {

        Route::post('forgot-password', 'Api\Auth\ForgotPasswordController@sendResetPassword');

        Route::get('reset-password/{scope}/{token}', 'Api\Auth\ResetPasswordController@find');

        Route::post('reset-password', 'Api\Auth\ResetPasswordController@reset');

    });

    Route::group([
        'prefix' => 'study',
    ], function () {
        Route::get('/', 'Api\StudyController@index');
    });

    Route::group([
        'prefix' => 'admin',
    ], function () {

        Route::group([
            'prefix' => 'auth',
        ], function () {

            Route::post('register', 'Api\Admin\Auth\RegisterController@registerPost');

            Route::post('login', 'Api\Admin\Auth\LoginController@login');

            Route::get('logout', 'Api\Admin\Auth\LoginController@logout')->middleware(['auth:api', 'scopes:admin']);

        });

        Route::group([
            'middleware' => ['auth:api', 'scopes:admin'],

        ], function () {

            Route::group([
                'prefix' => 'user',
            ], function () {
                Route::get('/', 'Api\Admin\Auth\UserController@index');
                Route::post('change-password', 'Api\Admin\Auth\UserController@changePassword');
            });

            Route::group([
                'prefix' => 'users',
            ], function () {

                Route::get('/', 'Api\Admin\UserController@index');
                Route::post('/create', 'Api\Admin\UserController@create');
                Route::get('/edit/{id}', 'Api\Admin\UserController@edit');
                Route::post('/update/{id}', 'Api\Admin\UserController@update');
                Route::post('/delete/{id}', 'Api\Admin\UserController@delete');

            });

            Route::group([
                'prefix' => 'study',
            ], function () {

                Route::get('/', 'Api\Admin\StudyController@index');
                Route::post('/create', 'Api\Admin\StudyController@create');
                Route::get('/edit/{id}', 'Api\Admin\StudyController@edit');
                Route::post('/update/{id}', 'Api\Admin\StudyController@update');
                Route::post('/delete/{id}', 'Api\Admin\StudyController@delete');

            });

            Route::group([
                'prefix' => 'category-article',
            ], function () {
                Route::get('/', 'Api\Admin\CategoryArticleController@index');
                Route::post('/create', 'Api\Admin\CategoryArticleController@create');
                Route::get('/edit/{id}', 'Api\Admin\CategoryArticleController@edit');
                Route::post('/update/{id}', 'Api\Admin\CategoryArticleController@update');
                Route::post('/delete/{id}', 'Api\Admin\CategoryArticleController@delete');
            });

            Route::group([
                'prefix' => 'category-event',
            ], function () {
                Route::get('/', 'Api\Admin\CategoryEventController@index');
                Route::post('/create', 'Api\Admin\CategoryEventController@create');
                Route::get('/edit/{id}', 'Api\Admin\CategoryEventController@edit');
                Route::post('/update/{id}', 'Api\Admin\CategoryEventController@update');
                Route::post('/delete/{id}', 'Api\Admin\CategoryEventController@delete');
            });

            Route::group([
                'prefix' => 'category-gallery',
            ], function () {
                Route::get('/', 'Api\Admin\CategoryGalleryController@index');
                Route::post('/create', 'Api\Admin\CategoryGalleryController@create');
                Route::get('/edit/{id}', 'Api\Admin\CategoryGalleryController@edit');
                Route::post('/update/{id}', 'Api\Admin\CategoryGalleryController@update');
                Route::post('/delete/{id}', 'Api\Admin\CategoryGalleryController@delete');
            });

            Route::group([
                'prefix' => 'tag-article',
            ], function () {
                Route::get('/', 'Api\Admin\TagArticleController@index');
                Route::post('/create', 'Api\Admin\TagArticleController@create');
                Route::get('/edit/{id}', 'Api\Admin\TagArticleController@edit');
                Route::post('/update/{id}', 'Api\Admin\TagArticleController@update');
                Route::post('/delete/{id}', 'Api\Admin\TagArticleController@delete');
            });

            Route::group([
                'prefix' => 'article',
            ], function () {
                Route::get('/', 'Api\Admin\ArticleController@index');
                Route::post('/create', 'Api\Admin\ArticleController@create');
                Route::get('/edit/{id}', 'Api\Admin\ArticleController@edit');
                Route::post('/update/{id}', 'Api\Admin\ArticleController@update');
                Route::post('/delete/{id}', 'Api\Admin\ArticleController@delete');
            });

            Route::group([
                'prefix' => 'event',
            ], function () {
                Route::get('/', 'Api\Admin\EventController@index');
                Route::post('/create', 'Api\Admin\EventController@create');
                Route::get('/edit/{id}', 'Api\Admin\EventController@edit');
                Route::post('/update/{id}', 'Api\Admin\EventController@update');
                Route::post('/delete/{id}', 'Api\Admin\EventController@delete');
            });

            Route::group([
                'prefix' => 'gallery',
            ], function () {
                Route::get('/', 'Api\Admin\GalleryController@index');
                Route::post('/create', 'Api\Admin\GalleryController@create');
                Route::get('/edit/{id}', 'Api\Admin\GalleryController@edit');
                Route::post('/update/{id}', 'Api\Admin\GalleryController@update');
                Route::post('/delete/{id}', 'Api\Admin\GalleryController@delete');
            });

            Route::group([
                'prefix' => 'mahasiswa-baru',
            ], function () {

                Route::get('/', 'Api\Admin\MahasiswaBaruController@index');
                Route::post('/create', 'Api\Admin\MahasiswaBaruController@create');
                Route::get('/edit/{id}', 'Api\Admin\MahasiswaBaruController@edit');
                Route::post('/update/{id}', 'Api\Admin\MahasiswaBaruController@update');
                Route::post('/delete/{id}', 'Api\Admin\MahasiswaBaruController@delete');

            });

            Route::group([
                'prefix' => 'mahasiswa-primagamas',
            ], function () {

                Route::get('/', 'Api\Admin\MahasiswaPrimagamasController@index');
                Route::post('/create', 'Api\Admin\MahasiswaPrimagamasController@create');
                Route::get('/edit/{id}', 'Api\Admin\MahasiswaPrimagamasController@edit');
                Route::post('/update/{id}', 'Api\Admin\MahasiswaPrimagamasController@update');
                Route::post('/delete/{id}', 'Api\Admin\MahasiswaPrimagamasController@delete');

            });

        });

    });

    Route::group([
        'prefix' => 'mahasiswa-primagamas',
    ], function () {

        Route::group([
            'prefix' => 'auth',
        ], function () {

            Route::post('forgot-password', 'Api\MahasiswaPrimagamas\Auth\ForgotPasswordController@sendResetPassword');

            Route::get('reset-password/{token}', 'Api\MahasiswaPrimagamas\Auth\ResetPasswordController@find');

            Route::post('reset-password', 'Api\MahasiswaPrimagamas\Auth\ResetPasswordController@reset');

            Route::post('register', 'Api\MahasiswaPrimagamas\Auth\RegisterController@registerPost');

            Route::post('login', 'Api\MahasiswaPrimagamas\Auth\LoginController@login');

            Route::get('logout', 'Api\MahasiswaPrimagamas\Auth\LoginController@logout')->middleware(['scopes:mahasiswa_primagamas', 'auth:api_mahasiswa_primagamas']);

        });

        Route::group([
            'prefix' => 'user',
            'middleware' => ['auth:api_mahasiswa_primagamas', 'scopes:mahasiswa_primagamas'],
        ], function () {
            Route::get('/', 'Api\MahasiswaPrimagamas\Auth\UserController@index');
            Route::post('change-password', 'Api\MahasiswaPrimagamas\Auth\UserController@changePassword');
            Route::post('update-profile', 'Api\MahasiswaPrimagamas\Auth\UserController@updateProfile');
        });

    });

    Route::group([
        'prefix' => 'mahasiswa-baru',
    ], function () {

        Route::group([
            'prefix' => 'auth',
        ], function () {

            Route::post('forgot-password', 'Api\MahasiswaBaru\Auth\ForgotPasswordController@sendResetPassword');

            Route::get('reset-password/{token}', 'Api\MahasiswaBaru\Auth\ResetPasswordController@find');

            Route::post('reset-password', 'Api\MahasiswaBaru\Auth\ResetPasswordController@reset');

            Route::post('register', 'Api\MahasiswaBaru\Auth\RegisterController@registerPost');

            Route::post('login', 'Api\MahasiswaBaru\Auth\LoginController@login');

            Route::get('logout', 'Api\MahasiswaBaru\Auth\LoginController@logout')->middleware(['scopes:mahasiswa_baru', 'auth:api_mahasiswa_baru']);

        });

        Route::group([
            'prefix' => 'user',
            'middleware' => ['auth:api_mahasiswa_baru', 'scopes:mahasiswa_baru'],
        ], function () {
            Route::get('/', 'Api\MahasiswaBaru\Auth\UserController@index');
            Route::post('change-password', 'Api\MahasiswaBaru\Auth\UserController@changePassword');
            Route::post('update-profile', 'Api\MahasiswaBaru\Auth\UserController@updateProfile');
        });

    });

});
